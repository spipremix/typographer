# SpipRemix/Typographer

SpipRemix Typographer is a tool that provide microtypography corrections for SPIP contents.

It relies on [SpipRemix/Typography](https://gitlab.com/spipremix/typography), which itself relies on [JoliCode/JoliTypo](https://github.com/jolicode/JoliTypo) library.


## Installation

```sh
composer require spipremix/typographer
```

## Notes

This Typographer determines the default locale to use from SPIP environment. 
Il also calls a SPIP pipeline (`typography_rules`) to modify or complete the default used rules.

## Usage

```php
use SpipRemix/Typographer/Typographer;

// This will choose locale from globals or constant in usage in SPIP CMS,
// and rules to apply for that locale by calling `typography_rules` pipeline.
$typographer = new Typographer();

$htmlContent = $typographer->fix($htmlContent);
$stringContent = $typographer->fixString($stringContent);
```

You can specify the locale and rules to use :

```php
// Specify locale
$typographer = new Typographer('fr_FR');
// Or just a lang (a common mapping is done, see Typographer::MAP_LANG_TO_LOCALE)
$typographer = new Typographer('fr');
// Second parameter is for a specific rules list to use
$typographer = new Typographer('fr_FR', ['SmartQuotes', 'CurlyQuotes']);
```
