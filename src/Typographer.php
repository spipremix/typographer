<?php

namespace SpipRemix\Typographer;

use SpipRemix\Typography\Fixer;
use SpipRemix\Typography\Fixer\TildeAsNoBreakSpace;

class Typographer
{
    public const DEFAULT_RULES_LOCALE = 'en_GB';

    public const MAP_LANG_TO_LOCALE = [
        'en' => 'en_GB',
        'fr' => 'fr_FR',
        'de' => 'de_DE',
    ];

    /**
     * @var array HTML Tags to bypass (with SPIP editing syntax specificity)
     */
    private array $protectedTags = ['head', 'link', 'pre', 'code', 'cadre', 'frame', 'script', 'style'];

    private Fixer $fixer;

    /**
     * Constructeur.
     *
     * @param string|null $locale Langue (fr) ou Locale (fr_FR). Calculé selon le contexte sinon.
     * @param array|null  $rules  Liste de règles à appeler (FixerInterface). Calculé au mieux pour la locale sinon.
     */
    public function __construct(?string $locale = null, ?array $rules = null)
    {
        $locale = $this->chooseLocale($locale);
        $rules = $rules ?? $this->chooseRules($locale);
        $this->fixer = $this->createFixer($locale, $rules);
    }

    public function fix(string $content): string
    {
        $fixed = $this->fixer->fix($content);

        return $fixed;
    }

    public function fixString(string $content): string
    {
        $fixed = $this->fixer->fixString($content);

        return $fixed;
    }

    /**
     * Retourne une locale à utiliser.
     * Utilisera par défaut la valeur de la globale `spip_lang`.
     */
    private function chooseLocale(?string $locale = null): string
    {
        if (empty($locale)) {
            $locale = $GLOBALS['spip_lang'] ?? _LANGUE_PAR_DEFAUT ?? self::DEFAULT_RULES_LOCALE;
        }
        $locale = self::MAP_LANG_TO_LOCALE[$locale] ?? $locale;

        return $locale;
    }

    /**
     * Retourne la liste des règles de typo à appliquer pour cette locale.
     *
     * Si on ne trouve pas directement,
     * - on applique celles de la locale spécifiée pour la langue de base de cette locale (ie: fr_CH => fr => fr_FR).
     * - sinon celles de la typo anglaise.
     */
    private function chooseRules(string $locale): array
    {
        $lang = Fixer::getLanguageFromLocale($locale);
        $list = Fixer::DEFAULT_RULES_BY_LOCALE;
        $rules = $list[$locale] ?? $list[self::MAP_LANG_TO_LOCALE[$lang] ?? null] ?? $list[self::DEFAULT_RULES_LOCALE] ?? [];
        $rules = [TildeAsNoBreakSpace::class, ...$rules];
        $rules = pipeline(
            'typography_rules',
            [
                'args' => [
                    'locale' => $locale,
                    'lang' => $lang,
                ],
                'data' => $rules,
            ]
        );

        return $rules;
    }

    private function createFixer(string $locale, array $rules): Fixer
    {
        $fixer = new Fixer($rules);
        $fixer->setLocale($locale);
        $fixer->setProtectedTags($this->protectedTags);

        return $fixer;
    }
}
