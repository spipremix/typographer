<?php

$finder = PhpCsFixer\Finder::create()
    ->in(array(__DIR__.'/src', __DIR__.'/tests'))
;


$config = new PhpCsFixer\Config();
$config
    ->setRules(array(
        '@Symfony' => true,
        'array_syntax' => array('syntax' => 'short'),
        'ordered_imports' => true
    ))
    ->setFinder($finder)
;

return $config;